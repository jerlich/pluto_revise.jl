module pluto_revise

using GLM

foo(x) = x * 3

foo(x::AbstractString) = "hello $x"



export foo

end