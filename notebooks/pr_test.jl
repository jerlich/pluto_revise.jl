### A Pluto.jl notebook ###
# v0.19.11

using Markdown
using InteractiveUtils

# ╔═╡ 0cd67d7a-3478-11ed-3c8a-f15392bfc0aa
begin
	import Pkg
	Pkg.activate(".")
end

# ╔═╡ 8b148c96-d68c-4216-be44-4e29cae27442
begin
	using Revise
	using pluto_revise
end

# ╔═╡ 3cd6e6cb-8dec-43e2-b6f5-c8aae3c89361
foo("a")

# ╔═╡ Cell order:
# ╠═0cd67d7a-3478-11ed-3c8a-f15392bfc0aa
# ╠═8b148c96-d68c-4216-be44-4e29cae27442
# ╠═3cd6e6cb-8dec-43e2-b6f5-c8aae3c89361
