# pluto_revise.jl

The goal of the project is to give an example of how one can use an _under development_ Julia package in a Pluto notebook.

I keep most of my git repositories in a `~/repos` so I will show the steps one can take to reproduce this process.

```
~ via ஃ v1.7.1
❯ cd repos

~/repos
❯ julia
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.7.1 (2021-12-22)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

(@v1.7) pkg> generate Foo
  Generating  project Foo:
    Foo/Project.toml
    Foo/src/Foo.jl

shell> cd Foo
/Users/jerlich/repos/Foo

(@v1.7) pkg> activate .
  Activating project at `~/repos/Foo`

(Foo) pkg>

shell> mkdir notebooks

shell> cd notebooks/
/Users/jerlich/repos/Foo/notebooks

(Foo) pkg> activate .
  Activating new project at `~/repos/Foo/notebooks`

(notebooks) pkg> add Revise
(notebooks) pkg> add Pluto
(notebooks) pkg> develop ..
   Resolving package versions...
    Updating `~/repos/Foo/notebooks/Project.toml`
  [794364bb] + Foo v0.1.0 `..`
    Updating `~/repos/Foo/notebooks/Manifest.toml`
  [794364bb] + Foo v0.1.0 `..`

(notebooks) pkg>
julia> using Pluto
julia> Pluto.run()
```

In Pluto make a new notebook _in the notebooks directory_.

Then follow the convention of `notebooks/pr_test.jl` in this repository.

